#include "encode.h"
#include <QHash>
#include <QDebug>

encode::encode() {
    m_frequency = new int[256];
    for(int count = 0; count < 256; ++count) {
        m_frequency[count] = 0;
    }
    m_trash = 0;

    pathToFile = bitString = "";
}

void encode::setFilePath(QString directory) {
    pathToFile += directory;
}

void encode::setReferences() {
    archive = new QFile(pathToFile);
    Q_ASSERT_X(archive->open(QIODevice::ReadOnly),
               Q_FUNC_INFO, "There are no file.");
    binaryFile = archive->readAll();
}

void encode::byteFrequency() {
    setReferences();
    for (int count = 0; count != binaryFile.size(); ++count) {
        ++m_frequency[uchar(binaryFile.at(count))];
    }
}

void encode::setBitString(QHash<uchar, QString> hash) {
    for(int count = 0; count != binaryFile.size(); ++count) {
        bitString += hash.value(uchar(binaryFile.at(count)));
    }

    if(bitString.size() % 8) {
        m_trash = 8 - (bitString.size() % 8);
    }

    bitString.append("0").repeated(m_trash);
}

int* encode::getOccur() const {
    return m_frequency;
}

int encode::getTrash() const {
    return m_trash;
}

QString encode::getBitString() {
    return bitString;
}
