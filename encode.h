#ifndef ENCODE_H
#define ENCODE_H

#include <QByteArray>
#include <QFile>
#include <QIODevice>
#include <QString>

class encode {
public:
    encode();

    void setFilePath(QString directory);
    void setReferences();
    void byteFrequency();
    void setBitString(QHash<uchar, QString> hash);

    int* getOccur() const;
    int getTrash() const;
    QString getBitString();

private:
    int* m_frequency, m_trash;
    QString pathToFile, bitString;

    QFile* archive;
    QByteArray binaryFile;
};

#endif // ENCODE_H
