#include "utils.h"
#include <QTime>

int main(void) {
    QTime cronometer;
    cronometer.start();

    encode* archive = new encode();
    tree* treeOfRuffles = new tree();

    archive->setFilePath("/home/swordfish/Pictures/Wallpapers/melissa-giraldo---miss-november-2009-betus-swimsuit-calendar-girl-wallpaper.jpg");
    archive->byteFrequency();

    treeOfRuffles->generateTree(archive);
    treeOfRuffles->generateHash(treeOfRuffles->getRoot());
    treeOfRuffles->MyRepresentation(treeOfRuffles->getRoot());

    archive->setBitString(treeOfRuffles->getHash());

    utils::debugMyProject(treeOfRuffles, treeOfRuffles->getRoot(), archive);

    qDebug("Execution time: %d ms", cronometer.elapsed());

    return 0;
}
