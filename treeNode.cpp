#include "treeNode.h"

treeNode::treeNode() {
    m_repetition = 0;
    m_leftChild = m_rightChild = 0;
}

treeNode::treeNode(int symbol, int repetition, treeNode* left, treeNode* right) {
    m_symbol = symbol;
    m_repetition = repetition;
    m_leftChild = left;
    m_rightChild = right;
}

bool treeNode::isLeaf() {
    if((m_leftChild == 0) && (m_rightChild == 0)) return true;
    return false;
}

int treeNode::getSymbol() const{
    return m_symbol;
}

int treeNode::getRepetition() const{
    return m_repetition;
}

treeNode *treeNode::getLeftChild() const{
    return m_leftChild;
}

treeNode *treeNode::getRightChild() const{
    return m_rightChild;
}
