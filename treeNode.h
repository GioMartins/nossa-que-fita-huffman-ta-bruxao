#ifndef TREENODE_H
#define TREENODE_H

class treeNode {
public:
    treeNode();
    treeNode(int symbol, int repetition, treeNode* left, treeNode* right);

    bool isLeaf();

    int getSymbol() const;
    int getRepetition() const;
    treeNode* getLeftChild() const;
    treeNode* getRightChild() const;

private:
    int m_symbol;
    int m_repetition;
    treeNode* m_leftChild;
    treeNode* m_rightChild;
};

#endif // TREENODE_H
