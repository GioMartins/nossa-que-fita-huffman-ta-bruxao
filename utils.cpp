#include "utils.h"

void utils::printOccur(encode*& MyFile) {
    qDebug() << endl << endl;
    for(int count = 0 ; count < 256 ; ++count) {
        if(MyFile->getOccur()[count]) {
            qDebug() << "ASCII:\t" << count << "\t"
                     << "Symbol:\t" << char(count) << "\t"
                     << "Repetitions:\t" << MyFile->getOccur()[count];
        }
    }
}

void utils::printList(tree*& MyTree) {
    qDebug() << endl << endl;
    for(int count = 0; count < MyTree->getList().size(); ++count) {
        qDebug() << "Symbol:\t" << MyTree->getList().at(count)->getSymbol() << "\t"
               "Repetitions:\t" << MyTree->getList().at(count)->getRepetition();
    }
    qDebug() << endl << endl;
}

void utils::printTree(treeNode * aNode, int level) {
    if(aNode != 0) {
        printTree(aNode->getRightChild(), level + 1);
        if(aNode->isLeaf())
            qDebug() << qPrintable(QString("\t").repeated(level))
                     << char(aNode->getSymbol()) << "/"
                     << aNode->getRepetition();
        else
            qDebug() << qPrintable(QString("\t").repeated(level)) << char(46);
        printTree(aNode->getLeftChild(), level + 1);
    }
}

void utils::printHash(tree*& MyTree) {
    QHash<uchar, QString>::const_iterator count;
    qDebug() << endl << endl;
    for(count = MyTree->getHash().constBegin();
        count != MyTree->getHash().constEnd(); ++count) {
        qDebug() << char(count.key()) << ": " << qPrintable(count.value()) << endl;
    }
}

void utils::printRepresentation(tree*& MyTree) {
    qDebug() << endl << endl
             << "Representing the tree: " << endl
             << qPrintable(MyTree->getRepresentation());
}

void utils::printBitString(encode *&MyFile) {
    qDebug() << endl << endl
             << "The codification in bits:" << endl
             << MyFile->getBitString();
}

void utils::debugMyProject(tree*& MyTree, treeNode* aNode, encode*& MyFile) {
    printOccur(MyFile);
    printList(MyTree);
    printTree(aNode);
    printHash(MyTree);
    printRepresentation(MyTree);
    //printBitString(MyFile);
}
