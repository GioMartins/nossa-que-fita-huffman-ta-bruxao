#ifndef TREE_H
#define TREE_H

#include <QHash>
#include <QList>
#include <QtAlgorithms>

#include "treeNode.h"
#include "encode.h"

class tree {
public:
    tree();

    void generateList(encode*& file);
    void generateTree(encode*& file);
    QHash<uchar, QString> generateHash(treeNode* aNode,QString tmp = "");

    QByteArray treeRepresentation(treeNode* aNode);
    void MyRepresentation(treeNode* aNode);

    treeNode* getRoot() const;
    QList<treeNode*> getList() const;
    QHash<uchar,QString> getHash() const;
    QByteArray getRepresentation() const;

    static bool comp(treeNode* left, treeNode*right);
private:
    QByteArray m_myRepresentation;
    QHash<uchar,QString> m_myHash;
    QList<treeNode*> m_myList;
    treeNode* m_root;
};

#endif // TREE_H
