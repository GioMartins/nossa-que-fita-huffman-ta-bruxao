#ifndef UTILS_H
#define UTILS_H

#include <QDebug>
#include "tree.h"

class utils {
public:
    static void printOccur(encode*& MyFile);
    static void printList(tree*& MyTree);
    static void printTree(treeNode * aNode, int level = 0);
    static void printHash(tree*& MyTree);
    static void printRepresentation(tree*& MyTree);
    static void printBitString(encode*& MyFile);

    static void debugMyProject(tree*& MyTree, treeNode * aNode, encode*& MyFile);
};

#endif // UTILS_H
