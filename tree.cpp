#include "tree.h"
#include "utils.h"

tree::tree() {
    m_root = 0;
}

void tree::generateList(encode*& file) {
    for(int count = 0; count < 256; ++count) {
        if(file->getOccur()[count]) {
            treeNode * tmp = new treeNode(count,file->getOccur()[count],NULL,NULL);
            m_myList.append(tmp);
        }
    }
    qSort(m_myList.begin(), m_myList.end(), comp);
}

void tree::generateTree(encode*& file) {
    generateList(file);
    Q_ASSERT_X(m_myList.length() >= 2, Q_FUNC_INFO, "It's impossible to build the tree.");
    while(m_myList.length() > 2) {
        qSort(m_myList.begin(), m_myList.end(), comp);
        treeNode* tmp = new treeNode(0, m_myList.at(0)->getRepetition()
                                      + m_myList.at(1)->getRepetition(),
                                        m_myList.at(0), m_myList.at(1));
        m_myList.removeFirst();
        m_myList.removeFirst();
        m_myList.insert(0,tmp);
    }
    if(m_myList.length() == 2) {
        m_root = new treeNode(0, m_myList.at(0)->getRepetition()
                               + m_myList.at(1)->getRepetition(),
                                 m_myList.at(0), m_myList.at(1));
        m_myList.removeFirst();
        m_myList.removeFirst();
        m_myList.insert(0,m_root);
    }
}

QHash<uchar, QString> tree::generateHash(treeNode* aNode, QString tmp) {
    Q_ASSERT_X(m_root, Q_FUNC_INFO, "Invalid Tree");
    if(aNode->isLeaf()) {
        m_myHash.insert(aNode->getSymbol(),tmp);
    } else {
        tmp += '0';
        generateHash(aNode->getLeftChild(), tmp);
        tmp = tmp.mid(0, tmp.length() - 1);
        tmp += '1';
        generateHash(aNode->getRightChild(), tmp);
    }
    return m_myHash;
}

QByteArray tree::treeRepresentation(treeNode *aNode) {
    QByteArray representation;
    if(aNode->isLeaf()) {
        if((aNode->getSymbol() == 0x21) || (aNode->getSymbol() == 0x2A)) {
            representation.append(0x21);
        }
        representation.append(aNode->getSymbol());
        return representation;
    } else {
        representation.append(0x2A);
        representation += treeRepresentation(aNode->getLeftChild()) +
                            treeRepresentation(aNode->getRightChild());
    }
    return representation;
}

void tree::MyRepresentation(treeNode *aNode) {
    m_myRepresentation = treeRepresentation(aNode);
    m_myRepresentation.remove(0,1);
}

treeNode* tree::getRoot() const{
    return m_root;
}

QList<treeNode*> tree::getList() const{
    return m_myList;
}

QHash<uchar, QString> tree::getHash() const{
    return m_myHash;
}

QByteArray tree::getRepresentation() const{
    return m_myRepresentation;
}

bool tree::comp(treeNode *left, treeNode *right) {
    if(left->getRepetition() != right->getRepetition()) {
        return left->getRepetition() < right->getRepetition();
    }
    return left->getSymbol() < right->getSymbol();
}

