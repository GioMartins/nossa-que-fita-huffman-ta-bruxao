#-------------------------------------------------
#
# Project created by QtCreator 2015-05-17T23:31:42
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Ruffles
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    tree.cpp \
    encode.cpp \
    utils.cpp \
    treeNode.cpp

HEADERS += \
    tree.h \
    encode.h \
    utils.h \
    treeNode.h
